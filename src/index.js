import React from "react";
import ReactDom from "react-dom";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Base from "./components/login/Base";
import Forget_pass from "./components/login/forget_pass";
import Main from "./components/main/Main";
import Reports from "./components/main/reports";
import Leaving from "./components/main/leaving";
import Setting from "./components/main/setting";
import Admin_main from "./components/admin/admin_main";
import Admin_setting from "./components/admin/admin_setting";
import Admin_report from "./components/admin/admin_reports";
import Admin_leaving from "./components/admin/admin_leaving";
import Error from "./components/Error";


function App() {

    return (
        <Router>
            <Switch>
                <Route path="/" component={Base} exact/>
                <Route path="/main" component={Main}/>
                <Route path="/reports" component={Reports}/>
                <Route path="/leaving" component={Leaving}/>
                <Route path="/setting" component={Setting}/>
                <Route path="/admin/main" component={Admin_main}/>
                <Route path="/admin/reports" component={Admin_report}/>
                <Route path="/admin/leaving" component={Admin_leaving}/>
                <Route path="/admin/setting" component={Admin_setting}/>
                <Route path="/forget_password" component={Forget_pass}/>
                <Route component={Error}/>
            </Switch>
        </Router>
    )
}
ReactDom.render(
        <App/>
    , document.getElementById("root"));
