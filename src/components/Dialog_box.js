import React from "react";
import "../css/Dialog_box.css";

class Dialog_box extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let dialog = (
            <div id="dialog_box" className={this.props.type}>
                <link rel="stylesheet"
                      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
                <div id="close" onClick={this.props.onClose}>
                    <i className="fa fa-times"></i>
                </div>
                <div id="dialog_icon">
                    {this.props.icon}
                </div>
                <div id="dialog_message">
                    {this.props.children}
                </div>
            </div>
        );

        if(! this.props.isOpen)
        {
            dialog=null;
        }

        return (
          <div>
              {dialog}
          </div>
        )
    }
}

export default Dialog_box