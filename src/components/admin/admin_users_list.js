import React from "react";
import Account from "../../images/account.png";
import "../../css/admin/admin_main.css";
import "../../css/General.css";
import axios from "axios";
import moment from "jalali-moment";

class Admin_users_list extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }

    componentWillMount() {

    axios
        .post('http://localhost:4040/give_users_info', {

        }, {
            headers: {
                Authorization: localStorage.getItem("token")
            }
        })
            .then(({data}) => {
                const  admin_reports = data.map((result) =>
                    <div className="users"
                    >
                        <img src={Account} id="account_avatar"/>
                        <h2>{result.name}</h2>
                        <h6>{result.status}</h6>
                    </div>
                );
                this.setState({
                    data: admin_reports
                })
            })
            .catch(error => {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    // window.location.pathname = "/";

            });
    }

    render() {
        return (
            <div id="admin_users_list" className="admin_box" dir="rtl">
                <h2 className="title_box">کارکنان</h2>
                <a href="">
                    {this.state.data}
                </a>
            </div>
        )
    }
}

export default Admin_users_list;