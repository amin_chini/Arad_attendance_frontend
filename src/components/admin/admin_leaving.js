import React from "react";
import axios from "axios"
import Admin_menu from "./admin_menu";
import "../../css/admin/admin_main.css";
import "../../css/General.css";
import "../../css/main/leaving_history.css";

class Admin_leaving extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            data: [],
        };
    }

    componentWillMount(){
        axios
            .post("http://localhost:4040/give_names", {

            })
            .then(({data}) => {
                console.log(data);
                this.setState({
                    users: data
                })
            });

        axios
            .post("http:192.168.10.124:4040/all_leavings", {

            })
            .then(({data}) => {
                const Leaving_report = data.map((leaving) =>
                    <div className="history_box">
                        <h4 className="history_box_name">{leaving.the_name}</h4>
                        <h4 className="history_box_req_date">در تاریخ  { leaving.date }</h4>
                        <h4 className="history_box_type">به صورت { leaving.type}</h4>
                        <h4 className="history_box_date">{leaving.from_date} تا {leaving.to_date}</h4>
                        <p className="history_describe">{leaving.description}</p>
                        <h4 className="history_result">{leaving.result}</h4>
                    </div>

                );
                this.setState({
                    data: Leaving_report
                })
            });
    }

    render() {
        const { data } = this.state;
        return (
            <div id="admin_main">
                <Admin_menu/>
                <div id="admin_part">
                    <div id="reports_users_list" className="admin_box" dir="rtl">
                        <h2 className="title_box">کارکنان</h2>
                        <div id="history_part">
                            <div className="history_box">
                                <h4 className="history_box_name">نام و نام خانوادگی</h4>
                                <h4 className="history_box_req_date">در تاریخ فلان</h4>
                                <h4 className="history_box_type">به صورت فلان</h4>
                                <h4 className="history_box_date">برای تاریخ فلان</h4>
                                <p className="history_describe">توضیحات</p>
                                <h4 className="history_result">نتیجه</h4>
                            </div>
                            {data}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Admin_leaving;
