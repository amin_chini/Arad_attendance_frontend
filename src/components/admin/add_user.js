import React from "react";
import axios from "axios";
import Dialog_box from "../Dialog_box";

class Add_user extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            name: null,
            type: "user",
            addisOpen: false,
            existOpen: false
        }
    }

    username = (event) => {
        event.preventDefault();
        this.setState(
            {
                [event.target.name]: event.target.value
            })
    };

    password = (event) => {
        event.preventDefault();
        this.setState(
            {
                [event.target.name]: event.target.value
            })
    };

    name = (event) => {
        event.preventDefault();
        this.setState(
            {
                [event.target.name]: event.target.value
            })
    };

    type = (event) => {
        event.preventDefault();
        this.setState(
            {
                [event.target.name]: event.target.value
            })
    };

    create_user = (event) => {
        event.preventDefault();
        if (this.state.type === "null") {
            alert("error");
        } else {
            axios
                .post("http://localhost:4040/add_user", {
                    add_user_name: this.state.username,
                    add_pass_word: this.state.password,
                    add_type: this.state.type,
                    info: this.state.name
                }, {
                    headers: {
                        Authorization: localStorage.getItem("token")
                    }
                })
                .then(({data}) => {
                    if (data === "User added") {
                        console.log("ok");
                        this.setState(
                            {
                                addisOpen: true,
                            });
                    }

                    else if(data === "User exist!"){
                        this.setState({
                            existOpen: true
                        })
                    }
                })
                .catch(error => {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                });
        }
    };

    render() {
        return (
            <div id="add_user">
                <form action="">
                    <br/><br/>
                    <h2>اضافه کردن کاربر</h2>
                    <br/><br/>
                    <input
                        type="text"
                        placeholder="نام کاربری"
                        className="inputbox"
                        required
                        minLength="4"
                        maxLength="32"
                        name="username"
                        onChange={this.username}
                    />
                    <br/><br/>
                    <input
                        type="text"
                        placeholder="نام"
                        className="inputbox"
                        required
                        minLength="4"
                        name="name"
                        onChange={this.name}
                    />
                    <br/><br/>
                    <input
                        type="password"
                        placeholder="گذرواژه"
                        className="inputbox"
                        required
                        minLength="4"
                        maxLength="32"
                        name="password"
                        onChange={this.password}

                    />
                    <br/>

                    <label htmlFor="add_user_type">نوع: </label>
                    <select value={this.state.type} onChange={this.type} name="type" id="add_user_type">
                        <option value="user">کاربر</option>
                        <option value="admin">مدیر</option>
                    </select>

                    <br/>
                    <input
                        type="submit"
                        className="submit"
                        value="تایید"
                        onClick={this.create_user}
                    />
                </form>
                <Dialog_box
                    isOpen={this.state.addisOpen}
                    onClose={(e) => this.setState({ addisOpen: false })}
                    type="success"
                    icon={<i className="fa fa-check"></i>}
                >
                    کاربر اضافه شد
                </Dialog_box>
                <Dialog_box
                    isOpen={this.state.existOpen}
                    onClose={(e) => this.setState({ existOpen: false })}
                    type="error"
                    icon={<i className="fa fa-times"></i>}
                >
                    این نام کاربری قبلا استفاده شده
                </Dialog_box>
            </div>
        )
    }
}

export default Add_user;