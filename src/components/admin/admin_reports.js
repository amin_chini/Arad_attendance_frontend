import React from "react";
import Admin_menu from "./admin_menu";
import {Link} from "react-router-dom";
import axios from "axios"
import "../../css/admin/admin_main.css";
// import "../../css/admin/admin_reports.css";
import "../../css/General.css";
import Account from "../../images/account.png";
import User_report from "./User_report";

class Admin_main extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            data: [],
            isOpen: false,
            username: null
        }
    }

    componentWillMount() {
        axios
            .post('http://localhost:4040/give_users_info', {

            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                const  admin_reports = data.map((result) =>
                    <div className="users"
                         onClick={(e) => this.setState({
                             isOpen: true,
                             username: result.user_name
                         })}
                    >
                        <img src={Account} id="account_avatar"/>
                        <h2>{result.name}</h2>
                        <h6>{result.status}</h6>
                    </div>
                );
                this.setState({
                    data: admin_reports
                })
            })
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                // window.location.pathname = "/";

            });
    }


    render() {
        return (
            <div id="admin_main">
                <Admin_menu/>
                <div id="admin_part" dir="rtl">
                    <div id="reports_users_list" className="admin_box" dir="rtl">
                        <h2 className="title_box">کارکنان</h2>
                            {this.state.data}

                    </div>
                </div>
                <User_report
                    isOpen={this.state.isOpen}
                    username={this.state.username}
                    onClose={(e) => this.setState({isOpen: false})}
                />
            </div>
        )
    }
}

export default Admin_main;