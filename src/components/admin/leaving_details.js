import React from "react";
import axios from "axios";
import "../../css/leaving_details.css"

class Leaving_details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        let value = (
            <div id="leaving_box">
                <div id="close" onClick={this.props.onClose}>
                    <i className="fa fa-times"></i>
                </div>
                <div id="in_box">
                    <h1>{this.props.name}</h1>
                    <div id="leaving_timing">
                        <h2 id="">{this.props.from}</h2>
                        <h2 id="">{this.props.to}</h2>
                    </div>
                    <h3>type</h3>
                    <p>{this.props.des}</p>
                    <textarea name="" className="inputbox" placeholder="توضیحات" cols="30" rows="10"></textarea>
                    <br/>
                    <button id="agree" className="submit">موافقت</button>
                    <button id="disagree" className="submit">مخالفت</button>
                </div>
            </div>
        );

        if (!this.props.isOpen) {
            value = null
        }

        return (
            <div>
                {value}
            </div>
        )
    }
}

export default Leaving_details;