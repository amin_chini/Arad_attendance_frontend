import React from "react";
import {Link} from "react-router-dom";
import Logo2 from "../../images/arad_logo_b.png";
import "../../css/admin/admin_menu.css";

class Admin_menu extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillMount() {

    }

    logout = () => {
        localStorage.setItem("username", null);
        localStorage.setItem("token", null);
        window.location.pathname = "/"
    };

    render() {
        return(
            <div id="admin_menu" dir="rtl">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
                <div id="sidebar_admin_menu">
                    <div id="branding">
                        <img id="logo" src={Logo2} alt=""/>
                    </div>
                    <hr/>
                    <nav>
                        <ul>
                            <li><Link to="/admin/reports"><i className="fa fa-table"></i> گزارشات</Link></li>
                            <li><Link to="/admin/setting"><i className="fa fa-wrench"></i> تنظیمات</Link></li>
                            <li><Link onClick={this.logout}><i className="fa fa-sign-out"></i> خروج</Link></li>

                        </ul>
                    </nav>
                </div>
            </div>
        )
    }

}

export default Admin_menu;