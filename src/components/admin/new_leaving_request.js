import React from "react";
import "../../css/General.css";
import "../../css/admin/admin_main.css";
import "../../css/main/reports_table.css"
import axios from "axios";
import Account from "../../images/account.png";
import Leaving_details from "./leaving_details";

class New_leaving extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            new_leaving_status: "هیچ درخواست جدیدی ارسال نشده است",
            isOpen: false
        }
    }

    componentWillMount() {
        axios
            .post('http://localhost:4040/all_leavings', {}, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                if (data !== null) {
                    this.setState({
                        new_leaving_status: null
                    })
                }
                console.log(data);
                const new_leaving = data.map((result) =>
                    <tr onClick={(e) => this.setState({
                        isOpen: true,
                        leaving_id: result.leave_id,
                        name: result.the_name,
                        from: result.from_date,
                        to: result.to_date,
                        des: result.description,
                    })}>
                        <td>
                            {result.the_name}
                        </td>
                        <td>
                            {result.from_date.slice(5, 10)}
                        </td>
                        <td>
                            {result.to_date.slice(5, 10)}
                        </td>
                        <td>
                            {result.type}
                        </td>
                    </tr>
                );
                this.setState({
                    data: new_leaving
                })
            })
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                // window.location.pathname = "/";

            });
    }


    render() {
        return (
            <div id="sum_box" className="admin_box">
                <h2 className="title_box">درخواست مرخصی</h2>
                <p>{this.state.new_leaving_status}</p>
                <table>
                    <tr>
                        <td>نام</td>
                        <td>از</td>
                        <td>تا</td>
                        <td>نوع</td>
                    </tr>
                    {this.state.data}
                </table>
                <Leaving_details
                    isOpen={this.state.isOpen}
                    onClose={(e) => this.setState({isOpen: false})}
                    id={this.state.leaving_id}
                    name={this.state.name}
                    from={this.state.from}
                    to={this.state.to}
                    des={this.state.des}

                />
            </div>

        )
    }
}

export default New_leaving;