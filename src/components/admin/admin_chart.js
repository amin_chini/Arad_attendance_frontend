import React from "react";
import Chart from "react-google-charts";
import axios from "axios";
import "../../css/admin/admin_main.css";
import Account from "../../images/account.png";

class Admin_chart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {
        axios
            .post('http://localhost:4040/give_users_info', {}, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                    this.setState({
                        data: data
                    })
                }
            )
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                // window.location.pathname = "/";

            });
    }


    render() {
        return (
            <div id="chart">
                {/*<Chart*/}
                {/*    width={'100%'}*/}
                {/*    height={400}*/}
                {/*    chartType="ColumnChart"*/}
                {/*    loader={<div>Loading Chart</div>}*/}
                {/*    data={[*/}
                {/*        ['نام', 'ساعت'],*/}
                {/*        [this.state.data.name[0], 125],*/}
                {/*        ['حسان امینی لو', 37],*/}
                {/*        ['محمد رستگار', 40],*/}
                {/*        ['سعید شیروی', 24],*/}
                {/*        ['پارسا حق شناس', 35],*/}
                {/*        ['عباس رضا رحیمی بشر', 32],*/}
                {/*    ]}*/}
                {/*    options={{*/}
                {/*        title: 'این هفته',*/}
                {/*        chartArea: {width: '60%'},*/}
                {/*        hAxis: {*/}
                {/*            title: 'نام',*/}
                {/*            minValue: 0,*/}
                {/*        },*/}
                {/*        vAxis: {*/}
                {/*            title: 'ساعت',*/}
                {/*        },*/}
                {/*        colors: ['ed0036'],*/}
                {/*    }}*/}

                {/*    legendToggle*/}
                {/*/>*/}
            </div>
        )
    }
}

export default Admin_chart;