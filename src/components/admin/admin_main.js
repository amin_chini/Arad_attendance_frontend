import React from "react";
import Admin_menu from "./admin_menu";
import Admin_chart from "./admin_chart";
import Admin_users_list from "./admin_users_list";
import Admin_hours_sum from "./admin_hours_sum";
import New_leaving from "./new_leaving_request";
import "../../css/admin/admin_main.css";
import "../../css/General.css";

class Admin_main extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return(
            <div id="admin_main">
                <Admin_menu />
                <div id="admin_part" dir="rtl">
                    {/*<Admin_chart className="admin_box"/>*/}
                    <Admin_users_list />
                    {/*<Admin_hours_sum />*/}
                    {/*<New_leaving />*/}
                </div>
            </div>
        )
    }
}

export default Admin_main;