import React from "react";
import Maplace from "maplace-js"
import Admin_menu from "./admin_menu";
import User_information from "../main/User_information";
import Change_username from "../main/Change_username";
import Change_password from "../main/Change_password";
import Add_user from "./add_user";
import "../../css/General.css";
import "../../css/admin/admin_setting.css";

class Admin_setting extends React.Component {
    render() {
        return (
            <div id="admin_main">
                <Admin_menu/>
                <div id="admin_part" dir="rtl">
                    <fieldset className="admin_setting">
                        <legend>تنظیمات شخصی</legend>
                        <User_information/>
                        <br/>
                        <hr/>
                        <br/>
                        <Change_username/>
                        <br/>
                        <hr/>
                        <br/>
                        <Change_password/>
                    </fieldset>
                    <fieldset className="admin_setting">
                        <legend>تنظیمات مدیریتی</legend>
                        <Add_user/>
                        <br/>
                        <hr/>
                        <br/>
                    </fieldset>
                </div>
            </div>
        )
    }
}

export default Admin_setting;