import React from "react";
import logo from "../images/arad_logo.png";
import "../css/error.css";
import "../css/General.css";

function Error(){
    return(
        <div id="error_box">
            <img src={logo} alt="arad data"/>
            <br/>
            <h1>Error: not found</h1>
        </div>
    )
}

export default Error