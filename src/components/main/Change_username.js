import React from "react";
import axios from "axios";
import Dialog_box from "../Dialog_box";

class Change_username extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("username"),
            new_username: null,
            password: null,
            isOpen: false,
            errorisOpen: false
        }
    }

    send_new_username = (event) =>{
        event.preventDefault();
        axios
            .post('http://localhost:4040/change_username', {
                old_user: this.state.username,
                new_user: this.state.new_username,
                pass: this.state.password,
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                console.log(data);
                if(data === "User name successfully changed!"){
                    localStorage.setItem("username", this.state.new_username);
                    this.setState({
                        isOpen: true,
                    })
                }
                else if(data === "Incorrect password!"){
                    this.setState({
                        errorisOpen: true
                    })
                }
            })
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                window.location.pathname = "/";
            });
    };

    new_username_change = (event) =>{
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    password = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        return (
            <div className="change_input" dir="rtl">
                <form action="">
                    <h2>تغییر نام کاربری</h2>
                    <br/><br/>
                    <input
                        type="text"
                        className="inputbox"
                        placeholder="نام کاربری جدید"
                        required
                        minLength="4"
                        maxLength="32"
                        name="new_username"
                        onChange={this.new_username_change}/>

                    <br/><br/>

                    <input
                        type="password"
                        className="inputbox"
                        placeholder="گذرواژه"
                        required minLength="4"
                        maxLength="32"
                        name="password"
                        onChange={this.password}/>
                    <br/>
                    <input
                        type="submit"
                        className="submit"
                        value="تایید"
                        onClick={this.send_new_username}/>

                </form>

                <Dialog_box
                    type="success"
                    icon={<i className="fa fa-check"></i>}
                    isOpen={this.state.isOpen}
                    onClose={(e)=> this.setState({isOpen: false})}
                >
                    انجام شد.
                </Dialog_box>

                <Dialog_box
                    type="error"
                    icon={<i className="fa fa-times"></i>}
                    isOpen={this.state.errorisOpen}
                    onClose={(e)=> this.setState({errorisOpen: false})}
                >
                    گذرواژه اشتباه است
                </Dialog_box>

            </div>
        )
    }
}

export default Change_username;