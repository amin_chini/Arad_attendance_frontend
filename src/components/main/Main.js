import React from "react";
import Sidebar_Menu from "./sidebar_menu.js";
import Manual_entering from "./manual_entering";
import Dialog_box from "../Dialog_box";
import Clock from 'react-live-clock';
import moment from "jalali-moment";
import axios from "axios";
import "../../css/General.css";
import "../../css/main/main.css";

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state =
            {
                username: localStorage.getItem("username"),
                date: moment().locale('fa').format("YYYY/M/D"),
                s_time: null,
                e_time: null,

                hour: moment().locale('fa').format('H'),

                year: moment().locale('fa').format('YYYY'),
                month: moment().locale('fa').format('M'),
                day: moment().locale('fa').format('D'),

                wday: moment().format('dddd'),

                welcome_message: "صبح بخیر",

                did_worked: false,
                user_status: null,
                display_status: null,

                isOpen: false,
                manual_open: false,

                h1: null,
                m1: null,
                h2: null,
                m2: null
            };
    }

    Click = () => {
        if (this.state.hour > 22 || this.state.hour < 7 || this.state.wday === "Friday" || this.state.wday === "Thursday") {
            alert("شرکت آراد دیتا شب ها و همچنین پنجشنبه و جمعه ها بسته است.");
        } else {
            if (this.state.did_worked === false) {
                if (this.state.user_status === "off") {
                    this.setState({
                        s_time: moment().format("LT")
                    });

                    axios
                        .post('http://localhost:4040/start_time', {
                            user_name: this.state.username,
                            date: moment().locale("fa").format("YYYY-MM-DD"),
                            s_time: moment().format("LT"),
                            day: this.state.wday
                        }, {
                            headers: {
                                Authorization: localStorage.getItem("token")
                            }
                        })
                        .then(({data}) => {
                            this.setState({user_status: data});
                            this.setState({display_status: "STOP"});
                        })
                        .catch(error => {
                            if (error.response.data === "Forbidden") {
                                localStorage.setItem("token", null);
                                localStorage.setItem("username", null);
                                window.location.pathname = "/";
                            }
                        });

                } else if (this.state.user_status === "on") {
                    this.setState({
                        e_time: moment().format("LT")
                    });

                    axios
                        .post('http://localhost:4040/end_time', {
                            user_name: this.state.username,
                            e_time: moment().format("LT"),
                            date: moment().locale("fa").format("YYYY-MM-DD"),
                            day: this.state.wday
                        }, {
                            headers: {
                                Authorization: localStorage.getItem("token")
                            }
                        })
                        .then(({data}) => {
                            console.log(data);
                            this.setState({user_status: data});
                            this.setState({display_status: "START"});
                            this.setState({did_worked: true})
                        })
                        .catch(error => {
                            if (error.response.status == 403) {
                                localStorage.setItem("token", null);
                                localStorage.setItem("username", null);
                                window.location.pathname = "/";
                            }
                        });
                }
            } else {
                this.setState({
                    isOpen: true
                })
            }
        }
    };

    change_to_manual = () => {
        this.setState({
            Manual_open: true
        })
    };

    manual_change = (e) => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    manual_timing = (e) => {
        e.preventDefault();
        axios
            .post('http://localhost:4040/start_time', {
                user_name: this.state.username,
                date: moment().locale("fa").format("YYYY-MMM-DD"),
                s_time: moment().format("LT"),
                day: this.state.wday
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({user_status: data});
            })
            .catch(error => {
                if (error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });
        this.setState({
            e_time: moment().format("LT")
        });

        axios
            .post('http://localhost:4040/end_time', {
                user_name: this.state.username,
                e_time: moment().format("LT"),
                date: moment().locale("fa").format("YYYY-M-DD"),
                day: this.state.wday
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                console.log(data);
                this.setState({user_status: data});
                this.setState({did_worked: true})
            })
            .catch(error => {
                if (error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });

    };

    componentWillMount() {
        console.log(localStorage.getItem("token"));
        axios
            .post("http://localhost:4040/check_work", {
                user_name: this.state.username,
                today: moment().locale("fa").format("YYYY-MM-DD")
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({
                    did_worked: data
                })
            })
            .catch(error => {
                if (error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });
        axios
            .post('http://localhost:4040/check_status', {
                username: this.state.username
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({user_status: data});
                if (data === "off") {
                    this.setState({display_status: "START"});
                } else if (data === "on") {
                    this.setState({display_status: "STOP"});
                }
                console.log(this.state.user_status);
            })
            .catch(error => {
                if (error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });
    }

    componentDidMount() {
        axios
            .post("http://localhost:4040/give_start_time", {
                user_name: this.state.username,
                today: moment().locale("fa").format("YYYY-MM-DD")
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({
                        s_time: data,
                    }
                );
            })
            .catch(error => {
                if (error.response.status == 403) {
                    console.log(error.response.data);
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });

        axios
            .post("http://localhost:4040/give_end_time", {
                user_name: this.state.username,
                today: moment().locale("fa").format("YYYY-MM-DD")
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({
                        e_time: data,
                    }
                );
            })
            .catch(error => {
                if (error.response.status == 403) {
                    console.log(error.response.data);
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
            });

        if (this.state.hour < 12 && this.state.hour >= 7) {
            this.setState({welcome_message: "صبح بخیر"});
        }
        if (this.state.hour >= 12 && this.state.hour < 16) {
            this.setState({welcome_message: "ظهر بخیر"});
        }
        if (this.state.hour >= 16 && this.state.hour < 20) {
            this.setState({welcome_message: "عصر بخیر"});
        }
    }

    render() {
        const display_timing1 = (
            <div id="started_at">شروع: {this.state.s_time}</div>
        );

        const display_timing2 = (
            <div id="end_at">پایان: {this.state.e_time}</div>
        );

        return (
            <div id="main_page">
                <Sidebar_Menu/>
                <div id="main_part" dir="rtl">
                    <br/>
                    <h1 className="welcome_message"> {this.state.welcome_message} خوش آمدید!</h1>
                    <div id="today">
                        <h2>امروز: </h2>
                        <time className="date">{this.state.date}</time>
                    </div>
                    <br/>
                    <button onClick={this.Click} id="time_submit">
                        <span>
                            <Clock id="time" format={'HH:mm:ss'} ticking={true} timezone={'ASIA/Tehran'}/>
                        </span>
                        <br/>
                        <h1>{this.state.display_status}</h1>
                    </button>
                    <a onClick={(e) => this.setState({ manual_open: true })}>
                        دستی وارد می‌ کنم
                    </a>
                    <Manual_entering
                        manual_open={this.state.manual_open}
                        onClose={(e) => this.setState({manual_open: false})}
                        onTiming={(e) => this.manual_timing}
                        onChange={(e) => this.manual_change}
                    />
                    <div id="day_details">
                        {display_timing1}
                        {display_timing2}
                    </div>
                    <Dialog_box
                        isOpen={this.state.isOpen}
                        onClose={(e) => this.setState({isOpen: false})}
                        type="error"
                        icon={<i className="fa fa-times"></i>}
                    >
                        در هر روز یکبار امکان ثبت زمان وجود دارد
                    </Dialog_box>
                </div>
            </div>
        )
    }
}