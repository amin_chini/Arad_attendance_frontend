import React from "react";
import "../../css/main/manual_entering.css";

class Manual_entering extends React.Component{
    constructor(props){
        super(props);

    }

    render() {

        let value = (
            <div id="manual_entering">
                <link rel="stylesheet"
                      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
                <div id="close" onClick={this.props.onClose}>
                    <i className="fa fa-times"></i>
                </div>
                <form action="">
                    <h4>دستی وارد می‌کنم</h4>
                    <input type="number" min="00" max="60" className="inputbox" placeholder="M" name="m1"/>:
                    <input type="number" min="07" max="23" className="inputbox" placeholder="H" name="h1"/>

                    <br/><br/>
                    <input type="number" min="00" max="60" className="inputbox" placeholder="M" name="m2"/>:
                    <input type="number" min="07" max="23" className="inputbox" placeholder="H" name="h2"/>
                    <br/>
                    <input type="submit" className="submit" value="تایید"/>
                </form>
            </div>
        );

        if(! this.props.manual_open){
            value = null;
        }

        return(
            <div>
                {value}
            </div>
        )
    }
}

export default Manual_entering;