import React from "react";
import moment from "jalali-moment";
import axios from "axios";
import {DatePicker} from "react-advance-jalaali-datepicker";
import Sidebar_Menu from "./sidebar_menu";
import Per_leaving from "./per_leavings";
import "../../css/General.css";
import "../../css/main/leaving.css";

class Leaving extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("username"),
            period: "day",
            leaving_type: null,
            describe: null,
            from: null,
            ok: "empty state",
            to: null,
            value: null,
            req_time: moment().locale("fa").format("LLLL")
        }
    }

    period = (event) => {
        event.preventDefault();
        this.setState(
            {leaving_type: event.currentTarget.value}
        )

    };

    type = (event) => {
        event.preventDefault();
        this.setState(
            {leaving_type: event.currentTarget.value}
        )
    };

    deserved = (event) => {
        event.preventDefault();
        this.setState(
            {leaving_type: event.currentTarget.value}
        )
    };

    cure = (event) => {
        event.preventDefault();
        this.setState(
            {leaving_type: event.currentTarget.value}
        )
    };

    without_salary = (event) => {
        event.preventDefault()
        this.setState({
            leaving_type: event.target.value
        })
    };

    DatePickerInput = (props) => {
        console.log({props});
        return <input className="inputbox dateinput" {...props}
        />
    };

    postarea = (event) => {
        event.preventDefault();
        this.setState(
            {[event.target.name]: event.target.value}
        )
    };

    send_data = (event) => {
        axios
            .post("http://localhost:4040/leaving", {
                user_name: this.state.username,
                type: this.state.leaving_type,
                time_type: this.state.period,
                from_date: this.state.from,
                to_date: this.state.to,
                description: this.state.describe,
                request_time: this.state.req_time

            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                // if(data === "success")
            })
            .catch(error => {
                if(error.response.data === "Forbidden") {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
        });
    };


    from_change = (unix, formatted) => {
        this.setState({from: formatted});
    };

    to_change = (unix, formatted) => {
        this.setState({to: formatted})
    };

    render() {
        return (
            <div id="main_page">
                <Sidebar_Menu/>
                <div id="main_part" dir="rtl">
                    <h1>مرخصی</h1>
                    <h2>درخواست مرخصی</h2>
                    <form action="" className="leaving_form">
                        <div className="clearfix">
                        <br/>
                        <br/>
                        <div id="rightside">
                            <label htmlFor="leaving_course" className="box_label">دوره: </label>
                            <br/>
                            <br/>
                            <select value={this.state.period} onChange={this.period} name="" id="leaving_course">
                                <option value="hour">ساعتی</option>
                                <option value="day">روزانه</option>
                            </select>
                            <br/>
                            <br/>

                            <label htmlFor="leaving_type_box" className="box_label">نوع مرخصی: </label>
                            <br/>
                            <div id="leaving_type_box">

                                <ul>
                                    <br/>
                                    <li>

                                        <input type="radio"
                                               className="radio"
                                               name="leaving_type"
                                               id="Deserved"
                                               value="Deserved"
                                               checked={this.state.leaving_type === "استحقاقی"}
                                               onChange={this.type}
                                        />
                                        <label className="leaving_type_radio" htmlFor="Deserved">استحقاقی</label>

                                        <div className="check"></div>
                                    </li>
                                    <br/>
                                    <li>
                                        <input type="radio"
                                               className="radio"
                                               id="Cure"
                                               name="leaving_type"
                                               value="Cure"
                                               checked={this.state.leaving_type === "استعلاجی"}
                                               onChange={this.type}/>
                                        <label className="leaving_type_radio" htmlFor="Cure">استعلاجی</label>
                                        <div className="check"></div>
                                    </li>
                                    <br/>
                                    <li>

                                        <input type="radio"
                                               className="radio"
                                               id="Without_salary"
                                               name="leaving_type"
                                               value="Without_salary"
                                               checked={this.state.leaving_type === "بدون حقوق"}
                                               onChange={this.type}
                                        />
                                        <label className="leaving_type_radio" htmlFor="Without_salary">بدون حقوق</label>
                                        <div className="check"></div>
                                    </li>
                                </ul>
                                <br/>
                            </div>
                        </div>
                        <div id="leftside">
                            <div id="duration">
                                <label htmlFor="from" className="box_label"> از </label>

                                <DatePicker
                                    id="to"
                                    value={this.state.from}
                                    className="dateinput"
                                    inputComponent={this.DatePickerInput}
                                    onChange={this.from_change}
                                    required
                                />

                                <label htmlFor="to" className="box_label"> تا </label>

                                <DatePicker
                                    id="to"
                                    value={this.state.from}
                                    className="dateinput"
                                    inputComponent={this.DatePickerInput}
                                    onChange={this.to_change}
                                    required
                                />
                            </div>
                            <label htmlFor="leaving_description" className="box_label">توضیحات</label>
                            <br/>
                            <textarea name="describe" id="leaving_description" cols="38" rows="10" className="inputbox"
                                      onChange={this.postarea}></textarea>
                            <br/>
                        </div>
                        </div>
                        <input type="submit" onClick={this.send_data} className="submit" value="تایید"
                               id="leaving_button"/>
                    </form>
                    <Per_leaving/>
                </div>

            </div>
        )
    }
}

export default Leaving;