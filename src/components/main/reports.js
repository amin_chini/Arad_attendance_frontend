import React from "react";
import Sidebar_Menu from "./sidebar_menu.js";
import Report_table from "./report_table";
import "../../css/General.css";

class Reports extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div id="main_page">
                <Sidebar_Menu />
                <div id="main_part">
                    <h1>گزارشات</h1>
                    <Report_table/>
                </div>
            </div>
        )
    }
}

export default Reports;