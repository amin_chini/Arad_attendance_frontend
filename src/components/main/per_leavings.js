import React from "react";
import axios from "axios";
import "../../css/main/leaving_history.css";

class Per_leaving extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: localStorage.getItem("username"),
            data: []
        }
    }

    componentWillMount() {
        axios
            .post("http://localhost:4040/check_leaving", {
                user_name: this.state.username
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                console.log(data);
                const Leaving_report = data.map((leaving) =>
                    <tr>
                        <td>{leaving.request_time}</td>
                        <td>{leaving.from_date}</td>
                        <td>{leaving.to_date}</td>
                        <td>{leaving.type}</td>
                        <td>{leaving.time_type}</td>
                        <td>{leaving.description}</td>
                        <td>{leaving.status}</td>
                        <td>{leaving.result}</td>
                    </tr>
                );

                this.setState({
                    data: Leaving_report
                })
            })
            .catch(error => {
                if(error.response.data === "Forbidden"){
                    console.log(error.response);
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                window.location.pathname = "/";
                    }
            });
    }

    render() {
        return (
            <div id="per_leaving">
                <h2>تاریخچه</h2>
                <div id="history_part">
                    <div className="history_box">
                        <table>
                            <tr>
                                <td>در</td>
                                <td>از</td>
                                <td>تا</td>
                                <td>نوع</td>
                                <td>بازه</td>
                                <td>توضیحات</td>
                                <td>وضعیت</td>
                                <td>نتیجه</td>
                            </tr>
                            {this.state.data}
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Per_leaving;