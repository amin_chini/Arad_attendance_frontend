import React from "react";
import {DatePicker} from "react-advance-jalaali-datepicker";
import axios from "axios";
import "../../css/main/reports_table.css";
import "../../css/General.css";

class Report_table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            from: null,
            to: null,
            username: localStorage.getItem("username"),
            sum: null,
            data: []
        };
    }

    DatePickerInput = (props) => {
        console.log({ props });
        return <input className="ffff" {...props}
        />
    };

    fromChange = (unix, formatted) => {
        this.setState({from: formatted});
    };

    toChange = (unix, formatted) => {
        this.setState({to: formatted})
    };

    report_send = (event) => {
        event.preventDefault();
        console.log(this.state.from);
        console.log(this.state.to);
        axios
            .post("http://localhost:4040/report", {
                user_name: this.state.username,
                s_date: this.state.from,
                e_date: this.state.to
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                this.setState({
                    sum: data.pop()
                });
                console.log(this.state.sum);
                const Report_result = data.map((result) =>
                    <tr>
                        <td>{result.date.slice(0, 10)}</td>
                        <td>{result.the_day}</td>
                        <td>{result.start_time}</td>
                        <td>{result.end_time}</td>
                        <td>{result.total_time}</td>
                    </tr>
                );
                this.setState({
                    data: Report_result
                });
            })
            .catch(error => {
                // if(error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    // window.location.pathname = "/";
                // }
            });
    };

    render() {
        const { data } = this.state;
        return (
            <div id="report_table" dir="rtl">

                <form method="post">
                    <fieldset>
                        <legend> نمایش بده</legend>
                        <label htmlFor="from">از </label>

                        <DatePicker
                            id="to"
                            value={this.state.from}
                            className=""
                            inputComponent={this.DatePickerInput}
                            onChange={this.fromChange}
                        />

                        <label htmlFor="to"> تا </label>

                        <DatePicker
                            id="to"
                            value={this.state.from}
                            className=""
                            inputComponent={this.DatePickerInput}
                            onChange={this.toChange}
                        />

                        <input
                            type="button"
                            onClick={this.report_send}
                            className="submit"
                            value="نمایش"
                        />
                    </fieldset>
                </form>
                <div id="table">

                    <table>
                        <tr>
                            <td>تاریخ</td>
                            <td>روز</td>
                            <td>ورود</td>
                            <td>خروج</td>
                            <td>مجموع</td>
                        </tr>
                        {this.state.data}
                    </table>
                </div>
                <div id="sum">
                    جمع ساعات: {this.state.sum}
                </div>
            </div>
        )
    }
}

export default Report_table;