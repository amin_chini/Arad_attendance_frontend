import React from "react";
import {Link} from "react-router-dom";
import $ from "jquery";
import ls from "local-storage";
import "../../css/main/menu.css";
import "../../css/General.css";
import "../../css/main/toggle_menu.css";
import Logo from "../../images/arad_logo.png";
import Account from "../../images/account.png";

class Sidebar_Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("username")
        }
    }

    menu = () =>{
            $(".menu").toggleClass("disp");
            if($(".menu").hasClass("disp")){
                $(".menu").delay(3000).fadeOut(1000)
            }
            else if($(".menu").css("display")=="none"){
                $(".menu").fadeIn(100);
                $(".menu").addClass("disp").delay(3000).fadeOut(1000);
            }

    };

    componentDidMount() {
        const path = window.location.pathname;
        if(path === "/admin/main"){
            document.getElementById("menu").style.background = "#000";
        }
    }

    exit = () => {
        localStorage.setItem("username", null);
        localStorage.setItem("token", null);
        window.location.pathname = "/";
    }

    render() {

        return (
            <div id="side_bar_menu" dir="rtl">
                <link rel="stylesheet"
                      href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>


                <div id="toggle_menu">
                    <i className="fa fa-bars" onClick={this.menu}></i>

                    <div className="menu">
                        <ul>
                            <li><Link to="/main"><i className="fa fa-home"></i></Link></li>
                            <li><Link to="/reports"><i className="fa fa-table"></i></Link></li>
                            <li><Link to="/setting"><i className="fa fa-wrench"></i></Link></li>
                            <li><Link onClick={this.exit}><i className="fa fa-sign-out"></i></Link></li>
                        </ul>
                    </div>

                </div>

                <div id="menu">
                    <div id="toggle">
                    </div>
                    <div id="branding">
                        <a href=""><img src={Logo} id="logo"/></a>
                    </div>
                    <hr/>
                    <div id="user">
                        <img src={Account} alt="" id={"account"}/>
                        <h1>{this.state.username}</h1>
                    </div>
                    <ul>
                        <li>
                            <Link to="/main" id="home">خانه</Link>
                        </li>
                        <li id="reports">
                            <Link to="/reports" id="reports">گزارشات</Link>
                        </li>
                        <hr/>
                        <li id="setting">
                            <Link to="/setting" id="setting">تنظیمات</Link>
                        </li>
                        <li>
                            <Link onClick={this.exit}>
                                خروج
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default Sidebar_Menu;