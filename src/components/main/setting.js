import React from "react";
import axios from "axios";
import Sidebar_Menu from "./sidebar_menu.js";
import User_information from "./User_information";
import Change_username from "./Change_username";
import Change_password from "./Change_password";
import "../../css/General.css";
import "../../css/main/setting.css";

class Setting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("username"),
        }
    }
    render() {
        return (
            <div id="main_page">
                <Sidebar_Menu/>
                <div id="main_part">
                    <h1>تنظیمات</h1>
                    <div id="setting_page">
                        <div id="rightside">
                            <User_information/>
                        </div>
                        <div id="leftside">
                            <Change_username/>
                            <Change_password/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Setting;