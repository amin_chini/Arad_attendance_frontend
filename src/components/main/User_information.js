import React from "react";
import axios from "axios";
import Account from "../../images/account.png";
import "../../css/General.css";
import Dialog_box from "../Dialog_box";



class User_information extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: null,
            username: localStorage.getItem("username"),
            isOpen: false,
        }
    }

    componentWillMount() {
        axios
            .post("http://localhost:4040/info", {
                user_name: this.state.username,
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                const {name, email} = data;
                this.setState(
                    {
                        email: email,
                        name: name
                    }
                )
            })
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                window.location.pathname = "/";
            });
    }

    send_email = (event) => {
        event.preventDefault();
        axios
            .post('http://localhost:4040/add_email', {
                user: this.state.user,
                email: this.state.email
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                if(data === "Email successfully added!"){
                    this.setState({
                        isOpen: true
                    })
                }
            })
            .catch(error => {
                if(error.response.status == 403) {
                    localStorage.setItem("token", null);
                    localStorage.setItem("username", null);
                    window.location.pathname = "/";
                }
        });
    };

    change = (event) => {
        event.preventDefault();
        this.setState({
                [event.target.name]: event.target.value
            }
        );
    };

    logout = () => {
        localStorage.setItem("username", null);
        localStorage.setItem("token", null);
        window.location.pathname = "/"
    };

    render() {
        return (
            <div id="user_info" dir="rtl">
                <br/><br/>
                <h2>اطلاعات کاربر</h2>
                <br/>
                <img src={Account} alt="" id="account"/>
                <a href=""><h2>{this.state.username}</h2></a><br/><br/>
                <h3>{this.state.name}</h3>
                <h4>{this.state.email}</h4>
                {/*<a href="" id="logout" onClick={this.logout}>خروج</a>*/}
                <input type="email" id="email" placeholder="افزودن/تغییر ایمیل" className="inputbox" name="email"
                       onChange={this.change}/>
                <input type="submit" className="submit" value="تایید" onClick={this.send_email}/>
                <Dialog_box
                    type="success"
                    icon={<i className="fa fa-check"></i>}
                    isOpen={this.state.isOpen}
                    onClose={(e)=> this.setState({isOpen: false})}
                >
                    انجام شد.
                </Dialog_box>
            </div>

        )
    }
}

export default User_information;