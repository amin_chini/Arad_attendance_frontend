import React from "react";
import "../../css/General.css"
import axios from "axios";
import Dialog_box from "../Dialog_box";

class Change_password extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("username"),
            old_password: null,
            new_password: null,
            isOpen: false,
            errorisOpen: false
        }
    }

    send_new_password = (event) =>{
        event.preventDefault();
        axios
            .post('http://localhost:4040/change_password', {
                old_pass: this.state.old_password,
                new_pass: this.state.new_password,
                username: this.state.username
            }, {
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            })
            .then(({data}) => {
                console.log(data);
            })
            .catch(error => {
                localStorage.setItem("token", null);
                localStorage.setItem("username", null);
                window.location.pathname = "/";
            });
    };

    old_password_change = (event) =>{
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    new_password_change = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        return (
            <div className="change_input" dir="rtl">
                <h2>تغییر گذرواژه</h2>
                <br/><br/>
                <form action="">
                    <input
                        type="password"
                        placeholder="گذرواژه قدیمی"
                        className="inputbox"
                        required
                        minLength="4"
                        maxLength="32"
                        onChange={this.old_password_change}
                        name="old_password"
                    />
                    <br/><br/>
                    <input
                        type="password"
                        placeholder="گذرواژه جدید"
                        className="inputbox"
                        required
                        minLength="4"
                        maxLength="32"
                        onChange={this.new_password_change}
                        name="new_password"
                    />
                    <br/>
                    <input
                        type="submit"
                        className="submit"
                        value="تایید"
                        onClick={this.send_new_password}
                    />
                </form>

                <Dialog_box
                    type="success"
                    icon={<i className="fa fa-check"></i>}
                    isOpen={this.state.isOpen}
                    onClose={(e)=> this.setState({isOpen: false})}
                >
                    انجام شد.
                </Dialog_box>

                <Dialog_box
                    type="error"
                    icon={<i className="fa fa-times"></i>}
                    isOpen={this.state.errorisOpen}
                    onClose={(e)=> this.setState({errorisOpen: false})}
                >
                    گذرواژه اشتباه است
                </Dialog_box>
            </div>
        )
    }
}

export default Change_password;