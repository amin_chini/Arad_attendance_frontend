import React from "react";
import logo from "../../images/arad_logo.png";
import Form from "./form";
import "../../css/login/login_box.css";
import "../../css/General.css"

function Login_box(){
    return(
        <div id="content_box" dir="rtl">
            <a href=""><img src={logo} alt=""/></a>
            <hr/>
            <h2 className="login_title">ورود</h2>
            <Form/>
        </div>
    )
}

export default Login_box;