import React from "react";
import axios from "axios";
import { Link } from "react-router-dom"
import ls from "local-storage";
import Keep from "./keep_signed";
import "../../css/login/form.css";
import "../../css/General.css";
import Dialog_box from "../Dialog_box";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: null,
            pass_word: null,
            isOpen: false
        };
    }

    submit = (event) => {
        event.preventDefault();
        axios
            .post('http://localhost:4040/login', {
                user_name: this.state.user_name,
                pass_word: this.state.pass_word
            })
            .then(({data}) => {
                const {result, type, token} = data;
                if(result === true){
                    localStorage.setItem("username", this.state.user_name);
                    localStorage.setItem("token", "Bearer " + token);
                    if(type === "admin"){
                        window.location.pathname = "/admin/reports";
                    }
                    else if(type === "user"){
                        window.location.pathname = "/main";
                    }
                }
                else{
                    this.setState({
                        isOpen: true
                    })
                }

            });
    };


    username_change = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    password_change = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        });

    };

    render() {
        return (
            <form method="post" onSubmit={this.submit}>
                <div id="username">
                    <input
                        type="text"
                        placeholder="نام کاربری"
                        className="textbox"
                        name="user_name"
                        maxLength="32"
                        minLength="4"
                        onChange={this.username_change}
                        required/>
                </div>

                <div>
                    <input
                        type="password"
                        placeholder="گذرواژه"
                        className="textbox"
                        name="pass_word"
                        maxLength="32"
                        minLength="4"
                        onChange={this.password_change}
                        required/>
                </div>

                <Link to="" className="forget_pass">رمز عبور را فراموش کردم!</Link>

                <br/><br/>
                {/*<Keep/>*/}

                <input
                    type="submit"
                    name="submit"
                    value="ورود"
                    className="submit_button"/>

                <div id="result"></div>
                <Dialog_box
                    isOpen={this.state.isOpen}
                    onClose={(e) => this.setState({ isOpen: false })}
                    type="error"
                    icon={<i className="fa fa-times"></i>}
                >
                    نام کاربری یا گذرواژه اشتباه است.
                </Dialog_box>
            </form>
        )
    }
}

export default Form;

// https://medium.com/@faizanv/authentication-for-your-react-and-express-application-w-json-web-tokens-923515826e0