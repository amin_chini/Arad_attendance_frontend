import React from "react";
import axios from "axios";
import Logo from "../../images/arad_logo.png";
import "../../css/General.css";

class Forget_pass extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: null,
            username: "aradadmin"
        }
    }

    email = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    send_email = () => {
        axios
            .post("http://localhost:4040", {
                user_name: this.state.username,
                email: this.state.email
            })
            .then(({data}) => {
                console.log(data);
            })

    };

    render() {
        return(
            <div id="box">
                <div id="content_box">
                    <form action="">
                        <img src={Logo} alt=""/>
                        <br/><br/>
                        <h3>ایمیل خود را وارد کنید، در صورت صحیح بودن آن گذرواژه جدید به شما ارسال خواهد شد</h3>
                        <br/><br/>
                        <input
                            type="email"
                            placeholder="ایمیل"
                            className="textbox"
                            name="email"
                            maxLength="32"
                            minLength="4"
                            onChange={this.email}
                            required/>

                        <input
                            type="submit"
                            onClick=""
                            className="submit"
                            value="تایید"
                            onSubmit={this.send_email}
                        />

                    </form>
                </div>
            </div>
        )
    }
}

export default Forget_pass;